# talk-yak-trimmers

This talk is for all those yak shavers (like myself) out there :wink:

## Tools

 - fzf
 - ag
 - exa
 - bat
 - ripgrep
 - oh-my-zsh
 - bash vs zsh
 - ctop
 - autojump